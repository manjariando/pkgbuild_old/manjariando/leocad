# Maintainer: George Rawlinson <george@rawlinson.net.nz>
# Contributor: Michael Straube <michael.straube@posteo.de>
# Contributor: Seth Schroeder <theking@kingdomofseth.com>
# Contributor: Sven Schneider <archlinux.sandmann@googlemail.com>

pkgname=leocad
pkgver=21.06
_library=20.03
pkgrel=1
pkgdesc="A CAD program for creating virtual LEGO models"
arch=('i686' 'x86_64')
url="http://leocad.org"
license=('GPL')
makedepends=('qt5-tools')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/leozide/leocad/archive/v${pkgver}.tar.gz"
        "${pkgname}-${_library}-library.zip::https://github.com/leozide/leocad/releases/download/v${pkgver}/Library-${_library}.zip"
        "https://metainfo.manjariando.com.br/${pkgname}/org.${pkgname}.metainfo.xml")
sha512sums=('6cc868d3da15f9fe003a70d3a8ebaf2b4e6562a8728652cb7fc9e3ab5ddd9a90c5e433fd4d37cc9f972dc314399b4edb34fe5d9cd47ad63bf7c5d71e04e3c662'
            'b339a07cb1e9fc5a35f108bba915a0363361b295ba82980550b7b4f82eaa43be45187cb6084cf123c65b52b2d89038c8abab847d0933f6166cac59404dda671a'
            '218fea13ebaa7306269484e337c336a202274ad3e9447ce564a13e9a4818cafeff290b3802baeed1f4002635b35309e5c3a3421f7ad29d402f495eeb1a18771f')

build() {
    cd "$pkgname-$pkgver"

    # create Makefile with QMake
    qmake-qt5 \
        INSTALL_PREFIX=/usr \
        DISABLE_UPDATE_CHECK=1 \
        LDRAW_LIBRARY_PATH=/usr/share/leocad \
        "$pkgname.pro"

    # build package
    make
}

package() {
    depends=('qt5-base' 'hicolor-icon-theme' 'gcc-libs' 'zlib')

    cd "$pkgname-$pkgver"

    # install to pkgdir
    make INSTALL_ROOT="$pkgdir" install

    # install parts library
    install -Dm644 "$srcdir/library.bin" -t "$pkgdir/usr/share/leocad"

    #Appstream
    install -Dm644 "${srcdir}/org.${pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/org.${pkgname}.metainfo.xml"
    rm -rf ${pkgdir}/usr/share/metainfo/leocad.appdata.xml
    mv "${pkgdir}/usr/share/applications/${pkgname}.desktop" "${pkgdir}/usr/share/applications/org.${pkgname}.desktop"
}
